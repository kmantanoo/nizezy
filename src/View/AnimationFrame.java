package View;

import java.awt.Dimension;

import javax.swing.JFrame;

import Model.Token;

public class AnimationFrame extends JFrame {
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private RobotPanel robotPanel;

	public AnimationFrame(int xAxis, int yAxis) {
		
		robotPanel = new RobotPanel(xAxis, yAxis);
		
		Dimension d = robotPanel.getPreferredSize();
		getContentPane().setPreferredSize(d);
		setTitle("Animation");
		add(robotPanel);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		pack();
		setVisible(true);
		
	}
	
	public void addObstacle(int xAxis, int yAxis) {
		
		robotPanel.makeFlower(xAxis, yAxis);
		revalidate();
		
	}
	
	
	
	public void addRobot(String robotName, int xAxis, int yAxis){
		
		robotPanel.makeDuck(robotName, xAxis, yAxis);
		revalidate();
		
	}
	
	
	
	public void moveRobot(String name, String direction) {
		
		robotPanel.moveDuck(name, direction);
		revalidate();
		
	}
}
