package Model;

import java.util.Stack;

public class NodeAssignment extends Node {
	private static final int valueCount = 2;
	public NodeAssignment(Stack<Token> syntaxValue) {
		super("assn");
		loadValue(syntaxValue, valueCount);
	}
	
	
}
