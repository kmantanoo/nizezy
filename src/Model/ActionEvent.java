package Model;


import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.StringTokenizer;

import javax.security.auth.callback.TextInputCallback;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import View.GUI;

public class ActionEvent implements ActionListener {
	
	private int value; 
	private int option; 
	private String content = null; 
	public String name = null; 
	private String str;
	private JFileChooser fileChooser = new JFileChooser(".");
	public GUI gui;
	
	String s ;
	
	public ActionEvent (GUI ui) {
		setGUI(ui);
	}
	
	
	public void setGUI(GUI g) {
		gui = g;
	}
	
	
	public void open(){
   	 
        value = fileChooser.showOpenDialog(gui); 
        if(value == JFileChooser.APPROVE_OPTION){
        	
           gui.TextInput.setText("");
         
           try
           {
              
              name = fileChooser.getSelectedFile().getPath();
              FileInputStream fin = new FileInputStream(fileChooser.getSelectedFile());
              
              FileChannel fchan = fin.getChannel();
              
              long fsize = fchan.size();
             
              
              ByteBuffer buff = ByteBuffer.allocate((int)fsize);
              
              fchan.read(buff);
              
              buff.rewind();
              
              String str = new String(buff.array());
             
              gui.TextInput.append(str);
              content = gui.TextInput.getText();
              fchan.close();
              fin.close();
           }
           catch(IOException ioe)
           {
              System.err.println("I/O Error on Open");
           }
          
           gui.setTitle(fileChooser.getSelectedFile().getAbsolutePath()+ " - Text Editor");
           str = fileChooser.getSelectedFile().getAbsolutePath();
        }
     }

    public void saveAs(){

    	 
        fileChooser.setDialogTitle("Save As"); 
        value = fileChooser.showSaveDialog(gui);
        if(value == JFileChooser.APPROVE_OPTION){
           try
           {
              
              FileOutputStream fout = new FileOutputStream(fileChooser.getSelectedFile() + ".txt");
             
              PrintWriter pw = new PrintWriter(fout);
              content = gui.TextInput.getText();
              name = fileChooser.getSelectedFile().getPath();
              
              StringTokenizer st=new StringTokenizer(content,System.getProperty("line.separator"));
              while(st.hasMoreTokens())
              {
                 pw.println(st.nextToken());
              }
             
              pw.close();
              
              fout.close();
           }
           catch(IOException ioe)
           {
              System.err.println ("I/O Error on Save");
           }
           
           gui.setTitle(fileChooser.getSelectedFile().getAbsolutePath() + " - Text Editor");
           str = fileChooser.getSelectedFile().getAbsolutePath();
        } 
     }

    
    
    
	@Override
	public void actionPerformed(java.awt.event.ActionEvent e) {
		// TODO Auto-generated method stub	
	}

}
