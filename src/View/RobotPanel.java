package View;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.swing.JPanel;

import Model.Token;

public class RobotPanel extends JPanel {
	
	private int fixSize = 25;
	private int columnCount;
	private int rowCount;
	private ArrayList<Grass> grassArray = new ArrayList<Grass>();
	private ArrayList<Flower> flowerArray = new ArrayList<Flower>();
	private LinkedHashMap<String, Duck> duckMap = new LinkedHashMap<>();
	private Boolean[][] positionIsEmpty;
	private static String[] directions = {"north", "west", "south", "east"};
	
	public RobotPanel(){
		
	}
	public RobotPanel(int x,int y){
		makeGrass(x,y);
		positionIsEmpty = new Boolean[x][y];
		for(Boolean[] b: positionIsEmpty){
			Arrays.fill(b, Boolean.TRUE);
		}
	}
	
	public void makeGrass(int s1 , int s2){
		columnCount=s1;
		rowCount=s2;
		Dimension d = new Dimension(columnCount*fixSize,rowCount*fixSize);
		setPreferredSize(d);
		setOpaque(true);
		
		for(int i = 0 ; i < rowCount ; i++){
			for(int j = 0; j < columnCount; j++) {
				Grass grass = new Grass();
				grass.setX((i%columnCount)*fixSize);
				grass.setY((j%rowCount)*fixSize);
				grassArray.add(grass);
			}
		}
	//	grassArray.get(0).getImage();
		repaint();
		
	}
	
	public void makeDuck(String name, int x , int y){
		positionIsEmpty[x-1][y-1] = false;
		Duck duck = new Duck(name, x, y);
		duckMap.put(duck.getName(), duck);
		repaint();
		
	}
	
	public void makeFlower(int x , int y){
		
		positionIsEmpty[x-1][y-1] = false;
		Flower fw = new Flower();
		fw.setX((--x%columnCount)*fixSize);
		fw.setY((--y%rowCount)*fixSize);
		flowerArray.add(fw);
		repaint();
		
	}

	 public void paint(Graphics g) {
	    	
	        super.paint(g);

	        Graphics2D g2d = (Graphics2D)g;
	        
	        for(int i = 0 ; i < grassArray.size() ; i++){
	            
	            g2d.drawImage(grassArray.get(i).getImage(),grassArray.get(i).getX(),grassArray.get(i).getY(), this);
	        	
	        }
	        
	        for(String key: duckMap.keySet()){
	            Duck d = duckMap.get(key);
	            g2d.drawImage(d.getImage(), d.getxPosition(),d.getyPosition(), this);
	        	
	        }
	        
	        for(int k = 0 ; k < flowerArray.size() ; k++){
	            
	            g2d.drawImage(flowerArray.get(k).getImage(),flowerArray.get(k).getX(),flowerArray.get(k).getY(), this);
	        	
	        }
	        Toolkit.getDefaultToolkit().sync();
	        g.dispose();
	    }
		
	public void moveDuck(String name , String direction){
		
		if(canMoveToDirection(name, direction)) {
			Duck temp = getDuck(name);
			
			int dx = 0;
			int dy = 0;
			
			if(direction.equals("north")){
				
				dy--;
				
			} else if (direction.equals("west")){
				
				dx--;
				
			} else if (direction.equals("south")){
				
				dy++;
				
			} else if (direction.equals("east")){
				
				dx++;
				
			}
			
			temp.move(dx, dy);
			
			repaint();
		} else {
			moveDuck(name, turnLeft(direction));;
		}
		
	}
	
	private Duck getDuck(String name) {
		
		return duckMap.get(name);
		
	}
	
	public boolean canMoveToDirection(String name, String direction){
		Duck d = getDuck(name);
		int x = d.getxCoordination();
		int y = d.getyCoordination();
		return direction.equals("north") ? positionIsEmpty[x][y-1] :
			   direction.equals("south") ? positionIsEmpty[x][y+1] :
				   direction.equals("east") ? positionIsEmpty[x+1][y] :
					   direction.equals("west") ? positionIsEmpty[x-1][y] : false;
	}
	
	public String turnLeft(String direction) {
		return directions[(Arrays.asList(directions).indexOf(direction)+1)%directions.length];
	}
}
