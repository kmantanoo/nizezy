package Model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ParseTable {
	private ArrayList<ArrayList<String>> parseData;
	private static String fileName = "/parsedata";
	private ArrayList<String> columns;
	
	public ParseTable() {
		parseData = new ArrayList<>();
		columns = new ArrayList<String>();
		
		try {
			loadDataFromFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void loadDataFromFile() throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(fileName)));
		try {
			String line = br.readLine();

			while (line != null) {
				if(line.matches("\\d+.*")){
					int index = Integer.parseInt(String.valueOf(line.subSequence(0, line.indexOf("&"))));
					line = line.replaceFirst("\\d+", "");
					ArrayList<String> lineData = getSplitData(line);
					try{
						parseData.get(index).addAll(lineData);
					} catch (Exception e ) {
						parseData.add(lineData);
					}
				} else {
					columns.addAll(getSplitData(line));
				}
				line = br.readLine();
			}
		} finally {
			br.close();
		}
	}
	
	private ArrayList<String> getSplitData(String line) {
		ArrayList<String> lineData = new ArrayList<String>();
		int currentIndex = line.indexOf("&"), nextIndex;
		while(currentIndex < line.length()) {
			if(line.indexOf('&', currentIndex+1)>0) nextIndex = line.indexOf("&", currentIndex+1);
			else nextIndex = line.length();
			String data = line.substring(currentIndex, nextIndex);
			if(data.contains("&")) data = data.replaceFirst("&", "");
			lineData.add(data);
			currentIndex = nextIndex;
		}
		
		return lineData;
	}
	
	public String getInfoFrom(int state, String column) {
		int columnIndex = 0; 
		while(columnIndex < columns.size()) {
			if(columns.get(columnIndex).equals(column)) break;
			else columnIndex++;
		}
		
		return parseData.get(state).get(columnIndex);
	}
	
	
}
