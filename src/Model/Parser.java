package Model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Stack;

public class Parser {
	private ProductionSets rules;
	private ParseTable parsingTable;
	private Stack<String> derivationSequence;
	private Stack<String> reduceRuleSequence;
	private Stack<String> mainStack;
	private Stack<String> derivations;
	private Stack<Token> syntaxValue;
	private Stack<Node> syntaxNode;
	private String[] compatWithSyntax = {"i", "v", "e", "w", "n", "s"};
	private String[] rulesList = {"1","3","4","5","6","7","8","9"};
	private Node n;
	private ArrayList<Token> input;
	private int indexOfData = 0;
	private boolean accepted = false;
	
	public Parser(ArrayList<Token> input) {
		this.input = input;
		rules = new ProductionSets();
		parsingTable = new ParseTable();
		derivationSequence = new Stack<String>();
		reduceRuleSequence = new Stack<String>();
		mainStack = new Stack<String>();
		derivations = new Stack<String>();
		syntaxValue = new Stack<Token>();
		syntaxNode = new Stack<Node>();
	}
	
	public boolean acceptCode() {
		input.add(new Token<String>("$", null, null));
		mainStack.push("0");
		String firstDerivation="";
		for(int i = 0 ; i < input.size()-1; i++) {
			firstDerivation += input.get(i).getFullInfo()+" ";
		}
		derivationSequence.add(firstDerivation);
		while(indexOfData<input.size() && accepted==false) {
			if(operateOneCycle()==false) return false;
		}
		return accepted;
	}
	
	private boolean operateOneCycle() {
		String column = input.get(indexOfData).getType();
		String result = parsingTable.getInfoFrom(Integer.parseInt(mainStack.peek()), input.get(indexOfData).getType());
		String command = result.substring(0,1);
		if(result.equals("")) return false;
		switch(command) {
		case "s":
			mainStack.push(column);
			mainStack.push(result.substring(1, result.length()));
			derivations.push(input.get(indexOfData).getFullInfo());
			if(Arrays.asList(compatWithSyntax).contains(input.get(indexOfData).getType())) syntaxValue.push(input.get(indexOfData));
			indexOfData++;
			return true;
		case "r":
			int ruleNumber = Integer.parseInt(String.valueOf(result.subSequence(1, result.length())));
			ProductionRule rule = rules.getRule(ruleNumber);
			Stack<String> ruleStack = rule.getGenerated();
			reduceRuleSequence.push(rule.toString());
			while(!ruleStack.isEmpty()) {
				while(!mainStack.peek().toString().equals(ruleStack.peek().toString())) {
					mainStack.pop();
				}
				mainStack.pop();
				ruleStack.pop();
				derivations.pop();
			}
			if(Arrays.asList(rulesList).contains(Integer.toString(ruleNumber))) createTree(ruleNumber);
			String newCommand = rule.getGenerator();
			int state = Integer.parseInt(mainStack.peek());
			String newResult = parsingTable.getInfoFrom(state, newCommand);
			mainStack.push(newCommand);
			derivations.push(newCommand);
			mainStack.push(newResult);
			Iterator<String> iter = derivations.iterator();
			String derivation = "";
			while(iter.hasNext()) {
				derivation += iter.next() + " ";
			}
			for(int i = indexOfData; i < input.size()-1; i++) {
				derivation += input.get(i).getFullInfo() + " ";
			}
			derivationSequence.push(derivation);
			return true;
			default:
				accepted = true;
				return true;
		}
		
	}
	
	
	private void createTree(int ruleNumber) {
		Node n = null;
		switch(ruleNumber) {
		case 1:
			n = new NodeBegin(syntaxValue, syntaxNode);
			break;
		case 3:
			n = new NodeSequence(syntaxNode);
			break;
		case 4:
			n = new NodeRobot(syntaxValue);
			break;
		case 5:
			n = new NodeObst(syntaxValue);
			break;
		case 6:
			n = new NodeMove(syntaxValue);
			break;
		case 7:
			n = new NodeAddition(syntaxValue);
			break;
		case 8:
			n = new NodeAssignment(syntaxValue);
			break;
		case 9:
			n = new NodeDo(syntaxValue, syntaxNode);
			break;
		}
		syntaxNode.push(n);
	}

	public String getDerivation() {
		Stack<String> reduced = (Stack<String>) reduceRuleSequence.clone();
		Stack<String> derivationed = (Stack<String>) derivationSequence.clone();
		
		derivationed.pop();
		String output="";
		while(!reduced.isEmpty()) {
			String red, der;
			red = reduced.pop();
			der = derivationed.pop();
			output += red + "\t" + String.format("%-40s", der) + "\n";
		}
		
		return output;
		
	}
	
	public Node getNode(){
		if(n==null) n = syntaxNode.pop();
		return n;
	}
}
