package Model;

public class Token<T> {
	private String type, charValue;
	private int intValue;
	private String fullInfo;
	private T dataType;

	public Token(String type, Object charValue, Object intValue) {
		super();
		this.type = type;
		if(charValue!=null) this.charValue = (String) charValue;
		if(intValue!=null) this.intValue = Integer.parseInt((String) intValue);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCharValue() {
		return charValue;
	}

	public void setCharValue(String charValue) {
		this.charValue = charValue;
		if(type.equals("i")) this.intValue = Integer.parseInt(charValue);
	}

	public int getIntValue() {
		return intValue;
	}

	public void setIntValue(int intValue) {
		this.intValue = intValue;
		if(type.equals("i")) this.charValue = Integer.toString(intValue);
	}
	
	public String toString() {
		return charValue==null ? type : charValue ;
	}
	
	public String getFullInfo() {
		return fullInfo;
	}
	
	public void setFullInfo(String fullInfo) {
		this.fullInfo = fullInfo;
	}
	
	public T getDataType() {
		return dataType;
	}
	
	
}
