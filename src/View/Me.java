package View;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class Me extends JFrame{
	public JPanel p1 = new JPanel();
	public JLabel lb1 = new JLabel("Miss  Natthanan   Kamonnatepisute");
	public JLabel lb2 = new JLabel("5410450078 	    Sec200");
	public JLabel lb3 = new JLabel("Create  Editor  for ");
	public JLabel lb4 = new JLabel("Associate Professor");
	public JLabel lb5 = new JLabel("Chuleerat  Jaruskulchai");
	
	
	
	public Me() {
		
		setSize(550, 350);
		setLocation(420, 100);
		setVisible(true);
		
		p1.setLayout(null);
		p1.setBounds(0, 0, 500, 500);
		p1.setBackground(new Color(175, 238, 238));
		
		lb1.setBounds(50, 30, 500, 50);
		lb1.setFont(new Font("Cooper Black", Font.ITALIC, 25));
		lb1.setForeground(new Color(160 ,32 ,240));
		
		lb2.setBounds(120, 70, 400, 50);
		lb2.setFont(new Font("Cooper Black", Font.ITALIC , 25));
		lb2.setForeground(new Color(122, 103, 238));
		
		lb3.setBounds(145, 130, 400, 50);
		lb3.setFont(new Font("Cooper Black", Font.ITALIC , 25));
		lb3.setForeground(new Color(34 ,139, 34));

		lb4.setBounds(140, 170, 400, 50);
		lb4.setFont(new Font("Cooper Black", Font.ITALIC, 25));
		lb4.setForeground(new Color(255, 215, 0));
		
		lb5.setBounds(110, 210, 400, 50);
		lb5.setFont(new Font("Cooper Black", Font.ITALIC, 25));
		lb5.setForeground(new Color(178, 34, 34)); 
		
		p1.add(lb1);
		p1.add(lb2);
		p1.add(lb3);
		p1.add(lb4);
		p1.add(lb5);
		
		add(p1);

	}

}
