package Model;

import java.util.Stack;

public class NodeSequence extends Node {
	private static final int childCount = 2;
	public NodeSequence(Stack<Node> syntaxNode) {
		super("seq");
		loadChild(syntaxNode, childCount);
	}
	
}
