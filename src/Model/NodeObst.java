package Model;

import java.util.Stack;

public class NodeObst extends Node {
	private static final int valueCount = 2;
	public NodeObst(Stack<Token> syntaxValue) {
		super("obst");
		loadValue(syntaxValue, valueCount);
	}
}
