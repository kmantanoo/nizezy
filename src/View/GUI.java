package View;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;














import Model.AnimationFrame;
import Model.Node;
import Model.NodeBegin;
import Model.Parser;
import Model.Position;
import Model.Robot;
import Model.TextLineNumber;
import Model.ActionEvent;
import Model.Tokenizer;

public class GUI extends JFrame{

	private JPanel contentPane;
	public JScrollPane inputScroll = new JScrollPane();
	public JTextArea TextInput = new JTextArea();
	String[] token,arrline; 
	public ActionEvent acEvent = new ActionEvent(this);
	int x,y,i=0;
	public JPanel panel = new JPanel();
	private Node node;
	private Parser p;

	ArrayList<Robot> robot = new ArrayList<Robot>();
	ArrayList<Position> Position = new ArrayList<Position>();

	public JMenuBar menuBar = new JMenuBar();
	public JPanel panel_1 = new JPanel();
	public JPanel panel_2 = new JPanel(new BorderLayout());
	//	public JPanel panel_3 = new JPanel();
	public JPanel panel_3 = new RobotPanel(10,10);
	public JPanel panel_4 = new JPanel(new BorderLayout());


	public TextLineNumber lineNum = new TextLineNumber(TextInput);

	public static JTextArea textoutputK = new JTextArea();
	public static JTextArea textoutputS = new JTextArea();
	public static JTextArea textoutputD = new JTextArea();
	public static JTextArea textoutputE = new JTextArea();

	public JScrollPane outputScroll1 = new JScrollPane();
	public JScrollPane outputScroll2 = new JScrollPane();
	public JScrollPane outputScroll3 = new JScrollPane();
	public JScrollPane outputScroll4 = new JScrollPane();

	public JButton btnNew = new JButton(new ImageIcon(getClass().getResource("page_2_copy.png")));
	public JButton btnOpen = new JButton(new ImageIcon(getClass().getResource("folder.png")));
	public JButton btnSave = new JButton(new ImageIcon(getClass().getResource("page_save.png")));
	public JButton btnRun = new JButton(new ImageIcon(getClass().getResource("control_play.png")));
	public JButton btnRunAni = new JButton(new ImageIcon(getClass().getResource("Runduck1.png")));



	JTabbedPane tabeP = new JTabbedPane();

	public JScrollPane scrollResult;


	public GUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(600, 650);
		setLocation(800, 40);
		setTitle("Editor");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);


		setMenu();	
		panel.setBounds(0, 0, 600, 650);
		panel.setBackground(new Color(255 ,228 ,225));
		contentPane.add(panel);
		panel.setLayout(null);


		setButton();

		panel_1.setBounds(0, 20, 950, 42);
		panel_1.setBackground(new Color(255 ,215, 0));
		panel.add(panel_1);
		panel_1.setLayout(null);


		panel_2.setBounds(22, 79, 550, 287);
		panel.add(panel_2);
		panel_2.setBackground(new Color(255 ,228 ,225));

		// Set Line Number //
		inputScroll = new JScrollPane(TextInput);
		inputScroll.setBounds(520, 85, 550, 520);
		getContentPane().add(inputScroll);
		lineNum = new TextLineNumber(TextInput);
		inputScroll.setRowHeaderView(lineNum);
		panel_2.add(inputScroll,BorderLayout.CENTER);

		//animation Panel
		panel_3.setBounds(508, 83, 550, 520);
		//panel.add(panel_3);
		panel_3.setBackground(new Color(255 ,228 ,181));


		outputScroll1 = new JScrollPane(textoutputK);
		outputScroll2 = new JScrollPane(textoutputS);
		outputScroll3 = new JScrollPane(textoutputD);
		outputScroll4 = new JScrollPane(textoutputE);

		textoutputK.setEditable(false);
		textoutputK.setText(" ");
		textoutputS.setEditable(false);
		textoutputS.setText(" ");


		//output panel
		panel_4.setBounds(22, 388, 550, 210);
		panel.add(panel_4);
		panel_4.setBackground(new Color(255 ,228 ,225));

		tabeP.addTab("Keyword Table", outputScroll1);
		tabeP.addTab("Symbol Table",outputScroll2);
		tabeP.addTab("Derivation Table",outputScroll3);
		tabeP.addTab("Error", outputScroll4);
		panel_4.add(tabeP , BorderLayout.CENTER);


		setVisible(true);

	}


	public GUI getInstance() {
		return this;
	}



	public void setMenu(){

		menuBar.setBounds(0, 0, 950, 30);
		menuBar.setBackground(new Color(118, 238 ,198));
		panel.add(menuBar);

		JMenu mnFile = new JMenu("  File  ");
		menuBar.add(mnFile);

		JMenuItem mntmNew = new JMenuItem("  New  ");
		mnFile.add(mntmNew);
		mntmNew.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				// TODO Auto-generated method stub
				dispose();
				clearText();
				new GUI();
			}
		});

		JMenuItem mntmOpen = new JMenuItem("  Open  ");
		mnFile.add(mntmOpen);
		mntmOpen.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				// TODO Auto-generated method stub
				acEvent.open();
			}
		});

		JMenuItem mntmSaveAs = new JMenuItem("  Save As  ");
		mnFile.add(mntmSaveAs);
		mntmSaveAs.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				// TODO Auto-generated method stub
				acEvent.saveAs();	
			}
		});

		JMenuItem mntmExit = new JMenuItem("  Exit  ");
		mnFile.add(mntmExit);
		mntmExit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				// TODO Auto-generated method stub
				dispose();	
			}
		});

		JMenu mnFunctions = new JMenu("  Functions  ");
		menuBar.add(mnFunctions);

		JMenuItem mntmTokenizer = new JMenuItem("  Tokenizer  ");
		mnFunctions.add(mntmTokenizer);
		mntmTokenizer.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				// TODO Auto-generated method stub
				String s ;
				Tokenizer t = new Tokenizer();
				s = TextInput.getText();
				t.setText(s,textoutputK,textoutputS,textoutputE);


			}
		});

		JMenu mnHelp = new JMenu("  Help  ");
		menuBar.add(mnHelp);

		JMenuItem mntmAcknow = new JMenuItem("  Acknowledge  ");
		mnHelp.add(mntmAcknow);
		mntmAcknow.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				// TODO Auto-generated method stub
				Image AckPic = new ImageIcon(this.getClass().getResource("a2.png")).getImage();
				JLabel AckLabel = new JLabel(new ImageIcon(AckPic));

				JFrame AckFrame = new JFrame();
				AckFrame.setSize(500,300);
				AckFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
				AckFrame.setLocationRelativeTo(null);
				AckFrame.setTitle("Acknowledge");
				AckFrame.setVisible(true);
				AckFrame.setResizable(false);
				AckFrame.add(AckLabel);

			}
		});

		JMenuItem mntmComm = new JMenuItem("  Comment  ");
		mnHelp.add(mntmComm);
		mntmComm.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				// TODO Auto-generated method stub
				Image ComPic = new ImageIcon(this.getClass().getResource("comment.png")).getImage();
				JLabel ComLabel = new JLabel(new ImageIcon(ComPic));

				JFrame AckFrame = new JFrame();
				AckFrame.setSize(500,270);
				AckFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
				AckFrame.setLocationRelativeTo(null);
				AckFrame.setTitle("Comment");
				AckFrame.setVisible(true);
				AckFrame.setResizable(false);
				AckFrame.add(ComLabel);
			}
		});


		JMenuItem mntmAboutMe = new JMenuItem("  About Me  ");
		mnHelp.add(mntmAboutMe);
		mntmAboutMe.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				// TODO Auto-generated method stub
				Me m = new Me();
			}
		});
	}

	public void clearText() {
		textoutputD.setText("");
		textoutputK.setText("");
		textoutputE.setText("");
		textoutputS.setText("");
	}

	public void setErrorText(String text) {
		textoutputE.setText(text);
		tabeP.setSelectedIndex(3);
	}


	public void setButton()
	{	
		btnNew.setBounds(3, 13, 25, 25);
		panel_1.add(btnNew);
		btnNew.addActionListener(new ActionListener() {


			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				// TODO Auto-generated method stub
				dispose();
				clearText();
				new GUI();
			}
		});


		btnOpen.setBounds(35, 13, 25, 25);
		panel_1.add(btnOpen);
		btnOpen.addActionListener(new ActionListener() {


			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				// TODO Auto-generated method stub
				acEvent.open();
			}
		});

		btnSave.setBounds(67, 13, 25, 25);
		panel_1.add(btnSave);
		btnSave.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				// TODO Auto-generated method stub
				acEvent.saveAs();
			}
		});


		btnRun.setBounds(99, 13, 25, 25);
		panel_1.add(btnRun);
		btnRun.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				// TODO Auto-generated method stub

				String s ;
				Tokenizer t = new Tokenizer();
				//textoutput1.setText(" ");
				s = TextInput.getText();
				try {
					t.setText(s,textoutputK,textoutputS,textoutputE);
					p = new Parser(t.getKeywordsTable());
					boolean acceptInput = p.acceptCode();
					node = p.getNode();
					node.setGUI(getInstance());
					textoutputD.setText(p.getDerivation());
				} catch (Exception stringIndexOutOfBoundException) {
					textoutputE.setText("Please coding first!");
				}


			}
		});

		btnRunAni.setBounds(131,13,25,25);
		panel_1.add(btnRunAni);
		btnRunAni.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(java.awt.event.ActionEvent arg0) {
				Node n;
				try {
					n = node.getInstance();
					Thread t = new Thread(n);
					t.start();
				} catch (CloneNotSupportedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

	}
}

