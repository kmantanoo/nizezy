package Control;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import Model.Position;
import Model.Robot;

public class Result extends JFrame {
	int x ,y,i,reline;
	String[] arrLine,token;
	ArrayList<Robot> robot = new ArrayList<Robot>();
	ArrayList<Position> obstacle = new ArrayList<Position>();
	JPanel panelButton;
	JButton btGo;
	int undo ;
	public Result(String[] ar) {
		arrLine = ar;	
		i=0;
		undo = 0;
		reline = 0;
		setLocation(300,0);
		setTitle("Robot");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);

		panelButton = new JPanel();
		btGo = new JButton("GO");
		btGo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				startGame();
				i+=1;
			}
		});
		panelButton.add(btGo);
		add(panelButton,BorderLayout.NORTH);
		startGame();
	}
	public void startGame(){
		token = arrLine[i].trim().split("\\s+");
		if(token[0].equals("begin")){
			x= Integer.parseInt(token[1]);
			y= Integer.parseInt(token[2]);
			x = 50+(x*15);
			y = 70+(y *15);
			setSize(x+60,y+80);
		}else if(token[0].equals("robot")){
			robot.add(new Robot(token[1],Integer.parseInt(token[2]),Integer.parseInt(token[3])));
		}else if(token[0].equals("obstacle")){
			obstacle.add(new Position(Integer.parseInt(token[1]),Integer.parseInt(token[2])));
		}else if(token[0].equals("move")){

	
			for(int i = 0 ; i < robot.size(); i++){
				if(robot.get(i).getName().equals(token[1])){
					robot.get(i).setPosition(token[2],Integer.parseInt(token[3]));
					boolean checkObstacle = false;
					for(int k = 0 ; k < obstacle.size(); k++){
						if(robot.get(i).getPositonX()==obstacle.get(k).getPositonX() && robot.get(i).getPositonY()==obstacle.get(k).getPositonY() ){
							checkObstacle = true;
						}

					}
					if(checkObstacle){
						JOptionPane.showMessageDialog(null,"Garbage collection  "); 
						obstacle.remove(i);
					}
			
				}
			}

		}else if(token[0].equals("halt")){
			setVisible(false);
		}




		repaint();
	}
	public void paint(Graphics  g){
		super.paintComponents(g);
		Graphics2D g2d = (Graphics2D)g;
		g2d.setColor(new Color(120,134,111));

		g2d.drawLine(50, 70, x, 70); //start x //table
		g2d.drawLine(50, y, 50, 70); //start y 
		g2d.drawLine(50, y, x, y); //end y
		g2d.drawLine(x, y, x, 70); //end x

		for(int b = 85; y>b; b = b+15)
		{g2d.drawLine(50, b, x, b);}

		for(int a = 65; x>a; a = a+15)
		{g2d.drawLine(a, y, a, 70);}

		
		for(int i = 0 ; i < obstacle.size(); i++){
			g2d.drawString("O", obstacle.get(i).getPositonX()+53, obstacle.get(i).getPositonY()+82);
		}
		for(int i = 0 ; i < robot.size(); i++){
			g2d.setColor(new Color(255,0,0));
			g2d.fillOval(robot.get(i).getPositonX()+51, robot.get(i).getPositonY()+71, 12, 12);
		}
		for(int i = 0 ; i < robot.size(); i++){
			g2d.setColor(new Color(255,255,255));
			g2d.drawString("R", robot.get(i).getPositonX()+53, robot.get(i).getPositonY()+82);
		}



	}
}
