package View;

import java.awt.Image;

import javax.swing.ImageIcon;

public class Flower {

    private Image image;
    private int x;
    private int y;
    private int dx;
    private int dy;

    public int getDx() {
		return dx;
	}

	public void setDx(int dx) {
		this.dx = dx;
	}

	public int getDy() {
		return dy;
	}

	public void setDy(int dy) {
		this.dy = dy;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Flower() {
    	
        image = new ImageIcon(this.getClass().getResource("flower1.png")).getImage();
        
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Image getImage() {
        return image;
    }

    public void move() {
        x += dx;
        y += dy;
    }

}
