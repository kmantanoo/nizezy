package View;

import java.awt.Image;

import javax.swing.ImageIcon;

public class Grass {

    private Image image;
    private int x;
    private int y;
    private int dx;
    private int dy;

    public Grass() {
    	
        image = new ImageIcon(this.getClass().getResource("grass.png")).getImage();
        
    }
    
    public void setX(int x){
    	this.x = x;
    }

    public int getX() {
        return x;
    }
    
    public void setY(int y) {
    	this.y = y;
    }

    public int getY() {
        return y;
    }

    public Image getImage() {
        return image;
    }

    public void move() {
        x += dx;
        y += dy;
    }

}
