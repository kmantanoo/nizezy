package Model;

import java.util.ArrayList;

public class ProductionSets {
	private ArrayList<ProductionRule> rules;
	public ProductionSets() {
		rules = new ArrayList<ProductionRule>();
		loadRules();
	}
	private void loadRules() {
		ProductionRule rZero = new ProductionRule("P�->P");
		ProductionRule rOne = new ProductionRule("P->b i i L h");
		ProductionRule rTwo = new ProductionRule("L->S ;");
		ProductionRule rThree = new ProductionRule("L->L S ;");
		ProductionRule rFour = new ProductionRule("S->r v T T");
		ProductionRule rFive = new ProductionRule("S->o T T");
		ProductionRule rSix = new ProductionRule("S->m v D T");
		ProductionRule rSeven = new ProductionRule("S->a T t v");
		ProductionRule rEight = new ProductionRule("S->v = T");
		ProductionRule rNine = new ProductionRule("S->d L u T > T");
		ProductionRule rTen = new ProductionRule("T->v");
		ProductionRule rEleven = new ProductionRule("T->i");
		ProductionRule rTwelve = new ProductionRule("D->n");
		ProductionRule rThirteen = new ProductionRule("D->s");
		ProductionRule rFourteen = new ProductionRule("D->e");
		ProductionRule rFifthteen = new ProductionRule("D->w");
		
		rules.add(rZero);
		rules.add(rOne);
		rules.add(rTwo);
		rules.add(rThree);
		rules.add(rFour);
		rules.add(rFive);
		rules.add(rSix);
		rules.add(rSeven);
		rules.add(rEight);
		rules.add(rNine);
		rules.add(rTen);
		rules.add(rEleven);
		rules.add(rTwelve);
		rules.add(rThirteen);
		rules.add(rFourteen);
		rules.add(rFifthteen);
		
	}
	
	public ProductionRule getRule(int ruleNumber) {
		return rules.get(ruleNumber);
	}
}
