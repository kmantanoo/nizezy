package View;

import java.awt.Image;

import javax.swing.ImageIcon;

import Model.Token;

public class Duck {

    private Image image;
    private String name;
    private int xCoordination;
    private int yCoordination;

    public Duck(String name, int x, int y) {
    	
    	setName(name);
    	xCoordination = x - 1;
    	yCoordination = y - 1;
        image = new ImageIcon(this.getClass().getResource("Runduck1.png")).getImage();
        
    }

    public int getxCoordination() {
		return xCoordination;
	}



	public void setxCoordination(int xCoordination) {
		this.xCoordination = xCoordination;
	}



	public int getyCoordination() {
		return yCoordination;
	}



	public void setyCoordination(int yCoordination) {
		this.yCoordination = yCoordination;
	}



	public String getName() {
		return name;
	}


	
	public void setName(String name) {
		this.name = name;
	}
	
	

    public Image getImage() {
        return image;
    }
    
    
    
    public int getxPosition() {
    	
    	return xCoordination*25;
 
    }
    
    
    
    public int getyPosition() {
    	
    	return yCoordination * 25;
    	
    }
    
    

    public void move(int dx, int dy) {
        xCoordination += dx;
        yCoordination += dy;
    }

}

