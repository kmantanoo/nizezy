package Model;

import java.awt.Dimension;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JFrame;
import View.RobotPanel;



public class AnimationFrame extends JFrame implements WindowListener {
	
	private RobotPanel robotPanel;
	private Node runner;
	
	public AnimationFrame(RobotPanel rbPanel, Node runner) {
		this.runner = runner;
		robotPanel = rbPanel;
		Dimension d = robotPanel.getPreferredSize();
		getContentPane().setPreferredSize(d);
		addWindowListener(this);
		setTitle("Animation");
		add(robotPanel);
//		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		pack();
		setVisible(true);
	}
	
	public void addObstacle(int x, int y) {
		robotPanel.makeFlower(x, y);
	}
	
	public void addRobot(String name, int x, int y) {
		robotPanel.makeDuck(name, x, y);
	}
	
	public void moveRobot(String name, String direction) {
		robotPanel.moveDuck(name, direction);
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		runner.setFinished(true);
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
