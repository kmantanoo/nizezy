package Model;

import java.util.ArrayList;
import java.util.LinkedList;

public class SyntaxTreeNode<T> {
	private T type;
	private LinkedList<SyntaxTreeNode> children;
	private ArrayList<Token> data;
	
	public SyntaxTreeNode(T data) {
		
		type = data;
		
	}
	
	
	
	public void addData(Token data){
		
		if(this.data==null) this.data = new ArrayList<Token>();
		this.data.add(data);
		
	}
	
	
	
	public ArrayList<Token> getData() {
		
		return data;
		
	}
	
	
	
	public void addChild(SyntaxTreeNode child) {
		
		if(children==null) children = new LinkedList<SyntaxTreeNode>();
		children.add(child);
		
	}
	
	
	
	public LinkedList<SyntaxTreeNode> getChildren() {
		
		return children;
		
	}
	
	
	public T getType() {
		
		return type;
		
	}
	
	
	
	public String toString() {
		
		return type.toString();
		
	}
}
