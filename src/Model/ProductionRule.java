package Model;

import java.util.Stack;
import java.util.StringTokenizer;

public class ProductionRule {
	private String generator;
	private Stack<String> generated;
	
	public ProductionRule(String rule) {
		generator = rule.substring(0, rule.indexOf("-"));
		generated = new Stack<String>();
		String gen = rule.substring(rule.indexOf(">")+1);
		StringTokenizer tokenizer = new StringTokenizer(gen);
		while(tokenizer.hasMoreTokens()) {
			generated.push(tokenizer.nextToken());
		}
	}

	public String getGenerator() {
		return generator;
	}

	public Stack<String> getGenerated() {
		return (Stack<String>) generated.clone();
	}
	
	public String toString() {
		String output = generator + "->";
		Stack<String> clonedGenerated = getGenerated();
		Stack<String> outputStack = new Stack<String>();
		while(!clonedGenerated.isEmpty()) {
			outputStack.push(clonedGenerated.pop());
		}
		while(!outputStack.isEmpty()) {
			output += outputStack.pop() + " ";
		}
		
		return output;
	}
	
	
}
