package Model;

import java.util.ArrayList;
import java.util.Stack;

public class NodeBegin extends Node {
	private static final int valueCount = 2;
	private static final int childCount = 1;
	public NodeBegin(Stack<Token> syntaxValue, Stack<Node> syntaxNode) {
		super("begin");
		loadChild(syntaxNode, childCount);
		loadValue(syntaxValue, valueCount);
	}
}
