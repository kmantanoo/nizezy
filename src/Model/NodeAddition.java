package Model;

import java.util.Stack;

public class NodeAddition extends Node {
	private static final int valueCount = 2;
	public NodeAddition(Stack<Token> syntaxValue) {
		super("add");
		loadValue(syntaxValue, valueCount);
	}
}
