package Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import View.GUI;
import View.RobotPanel;

public class Node implements Runnable{
	protected static HashMap<String, Integer> variableValMap;
	protected static volatile boolean finished=false;
	protected static GUI ui;
	private String nodeType;
	protected ArrayList<Node> childs;
	protected ArrayList<Token> values;
	protected static AnimationFrame frame;
	
	public Node() {
		childs = new ArrayList<Node>();
		values = new ArrayList<Token>();
		if(variableValMap==null) variableValMap = new HashMap<String, Integer>();
	}
	
	public void setGUI(GUI ui) {
		this.ui = ui;
	}
	
	public Node(String nodeType) {
		this.nodeType = nodeType;
		childs = new ArrayList<Node>();
		values = new ArrayList<Token>();
		if(variableValMap==null) variableValMap = new HashMap<String, Integer>();
	}
	
	public String getNodeType() {
		return nodeType;
	}
	
	public void setNodeType(String nodeType) {
		this.nodeType = nodeType;
	}
	
	public void newMap(String key, int value) {
		variableValMap.put(key, value);
	}
	
	public Integer getVarValue(String varName) {
		return variableValMap.get(varName);
	}
	
	public boolean haveChild() {
		return childs.size()==0 ? false: true;
	}
	
	public void traversal() throws InterruptedException {
		if(nodeType.equals("begin")) {
			int x, y;
			x = values.get(0).getIntValue();
			y = values.get(1).getIntValue();
			frame = new AnimationFrame(new RobotPanel(x, y), this);
			Thread.sleep(1500);
		}
		int i = 0;
		while((i<childs.size()) && !finished) {
			Node c = childs.get(i);
			if(c.haveChild()){
				c.traversal();
				i++;
			}
			else {
				if(!finished){
					operate(c);
					i++;
				}
			}
		}
		
		if(finished) {
			Thread.currentThread().interrupt();
		}
	}
	
	private void operate(Node c) throws InterruptedException {
		switch(c.getNodeType()) {
		case "add":
			performAdd(c);
			break;
		case "assn":
			performAssign(c);
			break;
		case "robot":
			performCreateRobot(c);
			break;
		case "move":
			performMoveRobot(c);
			break;
		case "obst":
			performAddObstacle(c);
			break;
		}
	}

	private void performAddObstacle(Node c) throws InterruptedException {
		int x,y;
		x = c.getValueAt(0).getIntValue();
		y = c.getValueAt(1).getIntValue();
		if(c.getValueAt(0).getType().equals("v")) {
			if(variableValMap.get(c.getValueAt(0).getFullInfo())==null) {
				ui.setErrorText(c.getValueAt(0).getFullInfo() + " not found!");
				finished=true;
			}
			if(!finished) x = variableValMap.get(c.getValueAt(0).getFullInfo());
		}
		if(c.getValueAt(1).getType().equals("v")) {
			if(variableValMap.get(c.getValueAt(1).getFullInfo())==null) {
				ui.setErrorText(c.getValueAt(1).getFullInfo() + " not found!");
				finished=true;
			}
			if(!finished) y = variableValMap.get(c.getValueAt(1).getFullInfo());
		}
		if(!finished) {
		frame.addObstacle(x, y);
		Thread.sleep(500);
		}
	}

	private void performMoveRobot(Node c) throws InterruptedException {
		String name, direction;
		int count;
		if(variableValMap.get(c.getValueAt(0).getFullInfo())==null) {
			ui.setErrorText(c.getValueAt(0).getFullInfo() + " is not created");
			finished = true;
		}
		if(!finished) {
			name = c.getValueAt(0).getFullInfo();
			direction = c.getValueAt(1).getFullInfo();
			count = c.getValueAt(2).getIntValue();
			if(c.getValueAt(2).getType().equals("v")) {
				if(variableValMap.get(c.getValueAt(2).getFullInfo())==null) {
					ui.setErrorText(c.getValueAt(2).getFullInfo() + " not found!");
					finished=true;
				}
				if(!finished) count = variableValMap.get(c.getValueAt(2).getFullInfo());
			}
			if(!finished) {
				int i = 0;
				while(i < count) {
					frame.moveRobot(name, direction);
					i++;
					Thread.sleep(500);
				}
			}
		}
	}

	private void performCreateRobot(Node c) throws InterruptedException {
		String name;
		int x, y;
		name = c.getValueAt(0).getFullInfo();
		x = c.getValueAt(1).getIntValue();
		y = c.getValueAt(2).getIntValue();
		if(c.getValueAt(1).getType().equals("v")) {
			if(variableValMap.get(c.getValueAt(1).getFullInfo())==null) {
				ui.setErrorText(c.getValueAt(1).getFullInfo() + " not found!");
				finished = true;
			}
			if(!finished) x = variableValMap.get(c.getValueAt(1).getFullInfo());
		}
		if(c.getValueAt(2).getType().equals("v")) {
			if(variableValMap.get(c.getValueAt(2).getFullInfo())==null) {
				ui.setErrorText(c.getValueAt(2).getFullInfo() + " not found!");
				finished = true;
			}
			if(!finished) y = variableValMap.get(c.getValueAt(2).getFullInfo());
		}
		if(!finished) {
			variableValMap.put(c.getValueAt(0).getFullInfo(), c.getValueAt(0).getIntValue());
			frame.addRobot(name, x, y);
			Thread.sleep(500);
		}
	}

	private void performAssign(Node c) {
		Token var;
		int value;
		var = c.getValueAt(0);
		value = c.getValueAt(1).getIntValue();
		if(c.getValueAt(1).getType().equals("v")) {
			if(variableValMap.get(c.getValueAt(1).getFullInfo())==null) {
				ui.setErrorText(c.getValueAt(1).getFullInfo() + " not found!");
				finished = true;
			}
			if(!finished) value = variableValMap.get(c.getValueAt(1).getFullInfo());
		}
		if(!finished) variableValMap.put(var.getFullInfo(), value);
	}

	private void performAdd(Node c) {
		Token transmitter;
		transmitter = c.getValueAt(0);
		int amount = transmitter.getIntValue(), receiver;
		if(transmitter.getType().equals("v")) {
			if(variableValMap.get(c.getValueAt(0).getFullInfo())==null) {
				ui.setErrorText(c.getValueAt(0).getFullInfo() + " not found!");
				finished = true;
			}
			if(!finished) amount = variableValMap.get(transmitter.getFullInfo());
		}
		if(!finished) {
			receiver = variableValMap.get(c.getValueAt(1).getFullInfo());
			receiver += amount;
			variableValMap.put(c.getValueAt(1).getFullInfo(), receiver);
		}
	}

	protected Token getValueAt(int index) {
		return values.get(index);
	}
	
	protected void loadValue(Stack<Token> syntaxValue, int count){
		for(int i = 0; i < count; i++){
			values.add(0, syntaxValue.pop());
		}
	}
	
	protected void loadChild(Stack<Node> syntaxNode, int count){
		int i = 0;
		while(i < count) {
			childs.add(0, syntaxNode.pop());
			i++;
		}
	}
	
	public void setValueMapping(HashMap<String, Integer> valueMap) {
		this.variableValMap = valueMap;
	}
	
	@Override
	public String toString() {
		return nodeType;
	}
	
	public void run() {
		try {
			traversal();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setFinished(boolean bool) {
		finished = bool;
	}
	
	public static HashMap<String, Integer> getVariableValMap() {
		return variableValMap;
	}

	public static void setVariableValMap(HashMap<String, Integer> variableValMap) {
		Node.variableValMap = variableValMap;
	}

	public ArrayList<Node> getChilds() {
		return childs;
	}

	public void setChilds(ArrayList<Node> childs) {
		this.childs = childs;
	}

	public ArrayList<Token> getValues() {
		return values;
	}

	public void setValues(ArrayList<Token> values) {
		this.values = values;
	}

	public Node getInstance() throws CloneNotSupportedException {
		Node result = new Node(getNodeType());
		result.setChilds(this.getChilds());
		result.setValueMapping(getVariableValMap());
		result.setValues(getValues());
		result.setGUI(this.ui);
		result.setFinished(false);
		return result;
	}
}
