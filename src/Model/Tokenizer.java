package Model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.swing.JTextArea;

public class Tokenizer {
	private String[] reserveToken = {"begin", "halt", "obstacle", "add", "to", "move", "north",
			"south", "east", "west", "robot", "do", "until", ";", "=", ">"};
	
	private JTextArea keywordPane;
	private JTextArea symbolPane;
	private JTextArea errorPane;
	private ArrayList<Token> keywordsTable;
	private String txt;
	private String errorMessage="";

	public Tokenizer() {
		super();
	}
	
	public Tokenizer(String data, JTextArea keywordPane, JTextArea symbolPane, JTextArea errorPane) {
		setText(data, keywordPane, symbolPane, errorPane);
	}
	
	public void setText(String s, JTextArea outputArea1 , JTextArea outputArea2 , JTextArea outputArea4)
	{
		keywordsTable = new ArrayList<Token>();
		
		keywordPane=outputArea1;
		symbolPane=outputArea2;
		errorPane=outputArea4;
		
		
		txt = s;
		CheckSentence();
	}

	private void CheckSentence() {
		String[] lines = txt.split("\n");
		for(int i = 0 ; i < lines.length; i++) {
			String replaced = "";
			if (verifyComment(lines[i])) {
				replaced = lines[i].replaceFirst("\\*-[^\n]*-\\*$", "");
			} else {
				errorMessage += "Line " + Integer.toString(i+1) + ": "+ "Everything to the right of *- is ignored as the comment is invalid because it does not end on the same line it starts on.\n";
			}
			StringTokenizer tokenizer = new StringTokenizer(replaced);
			while(tokenizer.hasMoreTokens()) {
				String currentToken = tokenizer.nextToken();
				Token tok = null;
				if(verifyToken(currentToken)){
					if(isNumeric(currentToken)){
						tok = new Token<Integer>("i", currentToken, currentToken);
						tok.setFullInfo(currentToken);
						keywordsTable.add(tok);
					} else if (isCommandOrSign(currentToken)) {
						tok = new Token<String>(currentToken.substring(0, 1), null, null);
						tok.setFullInfo(currentToken);
						keywordsTable.add(tok);
					} else if (isVariable(currentToken)) {
						tok = new Token<String>("v", currentToken, "0");
						tok.setFullInfo(currentToken);
						keywordsTable.add(tok);
					}
				} else {
					errorMessage += "Line " + Integer.toString(i+1) + ": " + currentToken + " is invalid token.\n";
				}
			}
		}

		errorPane.setText(errorMessage);
		printKeywords();
		printSymbol();
		
	}


	private boolean verifyComment(String currentToken){
//		if(currentToken.matches("\\*-[^\n]*-\\*$")){
//			return true;
//		} else 
		if (currentToken.matches("\\*-[^-\\*].*$")) {
			return false;
		}
		return true;
	}

	private boolean verifyToken(String currentToken) {
		if(isNumeric(currentToken) || isCommandOrSign(currentToken) || isVariable(currentToken)) return true;
		return false;
	}

	private boolean isNumeric(String currentToken) {
		return currentToken.matches("[0-9]{1,8}");
	}

	private boolean isVariable(String currentToken) {
		return currentToken.matches("[a-zA-Z]{1,8}");
	}

	private boolean isCommandOrSign(String currentToken) {
		return Arrays.asList(reserveToken).contains(currentToken.toLowerCase());
	}
	
	public ArrayList<Token> getKeywordsTable() {
		return keywordsTable;
	}
	
//	public static void main(String[] args) {
//		String data = "begin 40 60 \nobstacle 7 11 ;\n robot bob 5 10 ;\n move bob east 6 ;\n halt";
//		TheNewTokenizer tTok = new TheNewTokenizer();
//		tTok.setText(data, new JTextArea(), new JTextArea(), new JTextArea());
//	}
	
	private void printKeywords() {
		StringBuilder keyword = new StringBuilder();
		keyword.append("OUTPUT FOR PROGRAM \n");
		keyword.append("TYPE\tCH VALUE\tINT VALUE\t \n");
		keyword.append("====\t=======\t========\n");
		
		for(Token tk: keywordsTable) {
			String type, ch, in;
			type = tk.getType();
			ch = tk.getCharValue();
			in = Integer.toString(tk.getIntValue());
			keyword.append(type + "\t" + (ch==null?" ":ch) + "\t" + (in==null?" ":in) + "\n");
		}
		keywordPane.setText(keyword.toString());
	}
	
	private void printSymbol() {
		StringBuilder symbol = new StringBuilder();
		symbol.append("OUTPUT FOR PROGRAM \n");
		symbol.append("TYPE\tCH VALUE\tINT VALUE\t \n");
		symbol.append("====\t=======\t========\n");
		
		ArrayList<String> printedToken = new ArrayList<String>();
		for(Token tk: keywordsTable) {
			if(tk.getCharValue()!=null && !printedToken.contains(tk.getCharValue())) {
				String type, ch, in;
				type = tk.getType();
				ch = tk.getCharValue();
				in = Integer.toString(tk.getIntValue());
				symbol.append(type + "\t" + ch + "\t" + in + "\n");
				printedToken.add(tk.getCharValue());
			}
		}
		symbolPane.setText(symbol.toString());
	}
}
