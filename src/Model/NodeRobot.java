package Model;

import java.util.Stack;

public class NodeRobot extends Node {
	private static final int valueCount = 3;
	public NodeRobot(Stack<Token> syntaxValue) {
		super("robot");
		loadValue(syntaxValue, valueCount);
	}
	public String getRobotName() {
		return this.getValueAt(0).getFullInfo();
	}
	
	public int getI() {
		return getValueAt(1).getIntValue();
	}
	
	public int getJ() {
		return getValueAt(2).getIntValue();
	}
}
