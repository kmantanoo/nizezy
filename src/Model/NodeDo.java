package Model;

import java.util.Stack;

public class NodeDo extends Node {
	private static final int valueCount = 2;
	private static final int childCount = 1;
	public NodeDo(Stack<Token> syntaxValue, Stack<Node> syntaxNode) {
		super("do");
		loadChild(syntaxNode, childCount);
		loadValue(syntaxValue, valueCount);
	}
	
	@Override
	public void traversal() throws InterruptedException {
		int i, j;
		i = getValueAt(0).getIntValue();
		j = getValueAt(1).getIntValue();
		if(getValueAt(0).getType().equals("v")) {
			if(variableValMap.get(getValueAt(0).getFullInfo())==null) {
				ui.setErrorText(getValueAt(0).getFullInfo() + " not found!");
				finished = true;
			}
			if(!finished) i = variableValMap.get(getValueAt(0).getFullInfo());
		}
		if(getValueAt(1).getType().equals("v")) {
			if(variableValMap.get(getValueAt(1).getFullInfo())==null) {
				ui.setErrorText(getValueAt(1).getFullInfo() + " not found!");
				finished = true;
			}
			if(!finished) j = variableValMap.get(getValueAt(1).getFullInfo());
		}
		if(!finished) {
			do{
				childs.get(0).traversal();
				i = variableValMap.get(getValueAt(0).getFullInfo());
			} while (i <= j);
		}
	}
}
