package Model;

public class Error {
	private int line;
	private String token;
	private String message;
	public Error(String ms ,String word, int ln ) {
		message = ms;
		line = ln;
		token = word;
	}
	public String getTokenError(){
		return token;
	}
	public int getLine(){
		return line;
	}
	public String getMessage(){
		return message;
	}

	
}
