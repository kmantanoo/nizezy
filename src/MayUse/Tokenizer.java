package MayUse;

import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.JTextArea;

import Model.Token;

public class Tokenizer {
	String txt;
	String[] arrayLine;
	String[] arrayWord;
	int countLine;
	JTextArea keywordPane;
	JTextArea symbolPane;
	JTextArea errorPane;
	JTextArea derivationPane;
	int cont;
	ArrayList n = new ArrayList<>();
	ArrayList<Token> keywords = new ArrayList<Token>();
	ArrayList<String> s = new ArrayList<String>();
	String[] reserveWords = {"begin", "halt", "obstacle", "add", "to", "move", "north",
							"south", "east", "west", "robot", "do", "until", ";", "=", ">"};
	
	
	boolean error=true;
	
	boolean sDo=false;
	

	public void setText(String s, JTextArea outputArea1 , JTextArea outputArea2 , JTextArea outputArea4, JTextArea outputArea3)
	{
		keywordPane=outputArea1;
		symbolPane=outputArea2;
		errorPane=outputArea4;
		derivationPane=outputArea3;
		
		txt = s;
		CheckSentence();
//		CheckSyntax();
	}

	public boolean CheckSentence() 
	{try {
		

		txt = txt.toLowerCase();
		txt = txt.replaceAll("//.*", "\\t");
		arrayLine = txt.trim().split("\\n+");
		
		countLine = 0;
	
		keywordPane.append("OUTPUT FOR PROGRAM \n");
		keywordPane.append("TYPE\tCH VALUE\tINT VALUE\t \n");
		keywordPane.append("====\t=======\t========\n");
				
		symbolPane.append("OUTPUT FOR PROGRAM \n");
		symbolPane.append("TYPE\tCH VALUE\tINT VALUE\t \n");
		symbolPane.append("====\t=======\t========\n");
							
		for (String str : arrayLine)
		{
			countLine++;
			arrayWord = str.trim().split("\\s+");
			
			for(int i=0;i<arrayWord.length;i++){
				//Check Variable
				if(arrayWord[i].length()<=8){


					//Check Begin
					if(arrayWord[i].equalsIgnoreCase("begin"))
					{
						System.out.println("if begin");

						//check digit begin
						if(arrayWord[i+1].matches("-?\\d+(\\.\\d+)?"))
						{	//check digit and digit

							if(arrayWord[i+2].matches("-?\\d+(\\.\\d+)?"))
							{	

								if(!n.contains(arrayWord[i+1]))
								{
									n.add(arrayWord[i+1]);
									symbolPane.append("i \t"+arrayWord[i+1]+"\t"+arrayWord[i+1]+"\n");
									if(!n.contains(arrayWord[i+2]))
									{
										n.add(arrayWord[i+2]);
										symbolPane.append("i \t"+arrayWord[i+2]+"\t"+arrayWord[i+2]+"\n");
									}
								}

								keywordPane.append("b \n");
								keywordPane.append("i \t"+arrayWord[i+1]+"\t"+arrayWord[i+1]+"\n");
								keywordPane.append("j \t"+arrayWord[i+2]+"\t"+arrayWord[i+2]+"\n");
								keywords.add(new Token("b", null, null));
								keywords.add(new Token("i", arrayWord[i+1], arrayWord[i+1]));
								keywords.add(new Token("i", arrayWord[i+2], arrayWord[i+2]));
								//	symbolPane.append("i \t"+arrayWord[i+1]+"\t"+arrayWord[i+1]+"\n");
								//	symbolPane.append("i \t"+arrayWord[i+2]+"\t"+arrayWord[i+2]+"\n");

							}
							else
							{	
								errorPane.append("Error line "+countLine+"  :  behind keyword isn't digit \n");
								error = false;
							}					
						}
						else 
						{
							errorPane.append("Error line "+countLine+"  :  behind keyword isn't digit \n");
							error = false;		
						}
					}




					//Obstacle
					else if(arrayWord[i].equalsIgnoreCase("obstacle"))
					{
						System.out.println("if obstacle");
						if(arrayWord[i+1].matches("-?\\d+(\\.\\d+)?")){
							if(arrayWord[i+2].matches("-?\\d+(\\.\\d+)?")){
								if(arrayWord[i+3].equals(";"))
								{
									if(!n.contains(arrayWord[i+1]))
									{
										n.add(arrayWord[i+1]);
										symbolPane.append("i \t"+arrayWord[i+1]+"\t"+arrayWord[i+1]+"\n");
										if(!n.contains(arrayWord[i+2]))
										{
											n.add(arrayWord[i+2]);
											symbolPane.append("i \t"+arrayWord[i+2]+"\t"+arrayWord[i+2]+"\n");
										}
									}
									keywordPane.append("o \n");
									keywordPane.append("i \t"+arrayWord[i+1]+"\t"+arrayWord[i+1]+"\n");
									keywordPane.append("j \t"+arrayWord[i+2]+"\t"+arrayWord[i+2]+"\n");
									keywordPane.append("; \n");

									keywords.add(new Token("o", null, null));
									keywords.add(new Token("i", arrayWord[i+1], arrayWord[i+1]));
									keywords.add(new Token("i", arrayWord[i+2], arrayWord[i+2]));
									keywords.add(new Token(";", null, null));

									//	symbolPane.append("i \t"+arrayWord[i+1]+"\t"+arrayWord[i+1]+"\n");
									//	symbolPane.append("i \t"+arrayWord[i+2]+"\t"+arrayWord[i+2]+"\n");
								} 
							}
							else
							{
								errorPane.append("line "+countLine+"  :  behind keyword isn't digit\n");
								error = false;
							}
						}
						else {
							errorPane.append("line "+countLine+"  :  behind keyword isn't digit\n");
							error = false;
						}				
					}				



					//Robot
					else if(arrayWord[i].equalsIgnoreCase("robot")){
						System.out.println("if robot");
						//check name robot 
						if(arrayWord[i+1].matches("^[a-z]+$")){	
							//check digit
							if(arrayWord[i+2].matches("-?\\d+(\\.\\d+)?")){
								if(arrayWord[i+3].matches("-?\\d+(\\.\\d+)?")){
									if(arrayWord[i+4].equals(";")){


										if(!n.contains(arrayWord[i+1]))
										{
											n.add(arrayWord[i+1]);
											symbolPane.append("v \t"+arrayWord[i+1]+"\t"+"0 \n");
											if(!n.contains(arrayWord[i+2]))
											{
												n.add(arrayWord[i+2]);
												symbolPane.append("i \t"+arrayWord[i+2]+"\t"+arrayWord[i+2]+"\n");
											}
											if(!n.contains(arrayWord[i+3]))
											{
												n.add(arrayWord[i+3]);
												symbolPane.append("i \t"+arrayWord[i+3]+"\t"+arrayWord[i+3]+"\n");
											}
										}

										keywordPane.append("r \n");
										keywordPane.append("v \t"+arrayWord[i+1]+"\t"+"0"+"\n");
										keywordPane.append("i \t"+arrayWord[i+2]+"\t"+arrayWord[i+2]+"\n");
										keywordPane.append("j \t"+arrayWord[i+3]+"\t"+arrayWord[i+3]+"\n");
										keywordPane.append("; \n");

										keywords.add(new Token("r", null, null));
										keywords.add(new Token("v", arrayWord[i+1], "0"));
										keywords.add(new Token("i", arrayWord[i+2], arrayWord[i+2]));
										keywords.add(new Token("i", arrayWord[i+3], arrayWord[i+3]));
										keywords.add(new Token(";", null, null));

										//	symbolPane.append("v \t"+arrayWord[i+1]+"\t"+"0"+"\n");
										//	symbolPane.append("i \t"+arrayWord[i+2]+"\t"+arrayWord[i+2]+"\n");
										//	symbolPane.append("i \t"+arrayWord[i+3]+"\t"+arrayWord[i+3]+"\n");
									}
									else {
										errorPane.append("line "+countLine+"  :  behind keyword isn't digit \n");
										error = false;
									}
								}else {
									errorPane.append("line "+countLine+"  :  behind keyword isn't digit \n");
									error = false;
								}					
							}else {
								errorPane.append("line "+countLine+"  :  behind keyword isn't digit \n");
								error = false;
							}		
						}
					}


					// Move
					else if(arrayWord[i].equalsIgnoreCase("move")){	
						System.out.println("if move");
						if(arrayWord[i+1].matches("^[a-z]+$")){	
							if(arrayWord[i+2].matches("^[a-z]+$")){

								keywords.add(new Token("m", null, null));
								keywords.add(new Token("v", arrayWord[i+1], "0"));



								if(arrayWord[i+2].equals("east")){
									if(arrayWord[i+4].equals(";")){

										if(!n.contains(arrayWord[i+2]))
										{
											n.add(arrayWord[i+2]);
											symbolPane.append("v \t"+arrayWord[i+3]+"\t"+"0 \n");

										}
										keywordPane.append("m\n");
										keywordPane.append("v \t"+arrayWord[i+1]+"\t"+"0"+"\n");
										keywordPane.append("e\n");
										keywordPane.append("v \t"+arrayWord[i+3]+"\t"+"0"+"\n");
										keywordPane.append(";\n");

										//	symbolPane.append("v \t"+arrayWord[i+1]+"\t"+"0"+"\n");
										//	symbolPane.append("v \t"+arrayWord[i+3]+"\t"+"0"+"\n");
									}
									else {
										errorPane.append("line "+countLine+" : behind keyword isn't digit \n" );
										error = false;
									}
								}
								else if(arrayWord[i+2].equals("south")){
									if(arrayWord[i+4].equals(";")){
										keywordPane.append("m\n");
										keywordPane.append("v \t"+arrayWord[i+1]+"\t"+"0"+"\n");
										keywordPane.append("s\n");
										keywordPane.append("v \t"+arrayWord[i+3]+"\t"+"0"+"\n");
										keywordPane.append(";\n");							
									}
									else {
										errorPane.append("line "+countLine+" : behind keyword isn't digit \n" );
										error = false;
									}
								}
								else if (arrayWord[i+2].equals("north")){
									if(arrayWord[i+4].equals(";")){
										keywordPane.append("m\n");
										keywordPane.append("v \t"+arrayWord[i+1]+"\t"+"0"+"\n");
										keywordPane.append("n\n");
										keywordPane.append("v \t"+arrayWord[i+3]+"\t"+"0"+"\n");
										keywordPane.append(";\n");
									}
									else {
										errorPane.append("line "+countLine+" : behind keyword isn't digit \n" );
										error = false;
									}
								}

								else if (arrayWord[i+2].equals("west")){
									if(arrayWord[i+4].equals(";")){
										keywordPane.append("m\n");
										keywordPane.append("v \t"+arrayWord[i+1]+"\t"+"0"+"\n");
										keywordPane.append("w\n");
										keywordPane.append("v \t"+arrayWord[i+3]+"\t"+"0"+"\n");
										keywordPane.append(";\n");							
									}
									else {
										errorPane.append("line "+countLine+" : behind keyword isn't digit \n" );
										error = false;
									}
								}


								keywords.add(new Token(arrayWord[i+2].substring(0, 1), null, null));
								try{
									keywords.add(new Token("i", String.valueOf(Integer.parseInt(arrayWord[i+3])), arrayWord[i+3]));
								} catch (Exception e) {
									keywords.add(new Token("v", arrayWord[i+3], "0"));
								}
								keywords.add(new Token(";", null, null));

							}				
						}
					}



					// Add
					else if(arrayWord[i].equalsIgnoreCase("add")){
						System.out.println("if add");
						if(arrayWord[i+1].matches("^[a-z]+$")||arrayWord[i+1].matches("-?\\d+(\\.\\d+)?"))
							if(arrayWord[i+2].equals("to")){
								if(arrayWord[i+3].matches("^[a-z]+$")||arrayWord[i+1].matches("-?\\d+(\\.\\d+)?")){
									if(arrayWord[i+4].equals(";"));
								}

							}

						keywords.add(new Token("a", null, null));
						try{
							keywords.add(new Token("i", String.valueOf(Integer.parseInt(arrayWord[i+1])), arrayWord[i+1]));
						} catch (Exception e){
							keywords.add(new Token("v", arrayWord[i+1], "0"));
						}
						keywords.add(new Token("t", null, null));
						keywords.add(new Token("v", arrayWord[i+3], "0"));
						keywords.add(new Token(";", null, null));
					}



					//Check Variable
					else if(arrayWord[i].matches("^[a-z]+$")){
						if(arrayWord[i+1].equals("=")){
							if(arrayWord[i+2].matches("-?\\d+(\\.\\d+)?")){
								if(arrayWord[i+3].equals(";")){
									keywordPane.append("v \t"+arrayWord[i]+"\t"+"0"+"\n");
									keywordPane.append(arrayWord[i+1]+"\n");
									keywordPane.append("i \t"+arrayWord[i+2]+"\t"+arrayWord[i+2]+"\n");
									keywordPane.append(";\n");


									keywords.add(new Token("v", arrayWord[i], "0"));
									keywords.add(new Token("=", null, null));
									keywords.add(new Token("i", arrayWord[i+2], arrayWord[i+2]));
									keywords.add(new Token(";", null, null));

									//	symbolPane.append("v \t"+arrayWord[i]+"\t"+"0"+"\n");
									//	symbolPane.append("i \t"+arrayWord[i+2]+"\t"+arrayWord[i+2]+"\n");
								}
								else {
									errorPane.append("line "+countLine+"  :  behind keyword isn't digit \n");
								}
							}	
						}
					}

					else if(arrayWord[i].equalsIgnoreCase("do"))
					{
						sDo=true;
						keywordPane.append("d \n");
						keywords.add(new Token("d", null, null));
					}	

					else if(arrayWord[i].equalsIgnoreCase("until"))
					{
						sDo=false;
						keywordPane.append("u \n");
						keywords.add(new Token("u", null, null));
					}	

					else if(arrayWord[i].equalsIgnoreCase("halt"))
					{
						if(sDo){errorPane.append("line "+countLine+"  :  behind keyword isn't digit \n");}
						keywordPane.append("h \n");
						keywords.add(new Token("h", null, null));
					}	


				}else{errorPane.append("line "+countLine+"Error  :  Too Long\n");}
			}
		}
	} catch (Exception e) {
		// TODO: handle exception
	}
	return error;
}
	public void CheckSyntax() {

		derivationPane.append("RULES\t\t DERIVATION\n");
		arrayLine=txt.trim().split("\\n+");
		
		for (String A : arrayLine) {
			countLine++;
			arrayWord = A.trim().split("\\s+");
			txt= A.replaceAll("//.*", " ");
			for (int i=0;i<arrayWord.length;i++) {	
				txt = arrayWord[i];
				
				if(error)
				{
					if(txt.equals("begin"))
					{
						checkBegin(i);
						
					}else if(txt.equals("obstacle"))
					{	
						checkOb(i);
					}
					else if(txt.equals("robot")){
						checkRobot(i);
					}
					else if(txt.equals("move")){
						checkMove(i);
					}
					else if(txt.equals("add")) {
						checkAdd(i);
					}
				}
			}
		}
	}
 public void checkBegin(int i) {
	 
	derivationPane.append("P --> begin int int L halt\t");
	derivationPane.append("begin ");
	derivationPane.append(arrayWord[i+1]+" ");
	derivationPane.append(arrayWord[i+2]+" ");
	derivationPane.append("L ");
	derivationPane.append("halt\n");
	s.add("begin");
	s.add(arrayWord[i+1]+" ");
	s.add(arrayWord[i+2]+" ");
	s.add("L");
	s.add("halt");
//	System.out.println(arrayLine.length);
	if(arrayLine.length > 3){
		
			derivationPane.append("L>L S :\t\t");
			derivationPane.append("begin ");
			derivationPane.append(arrayWord[i+1]+" ");
			derivationPane.append(arrayWord[i+2]+" ");
			derivationPane.append("L ");
			derivationPane.append("S ");
//			s.set(i+1, "halt");
			derivationPane.append("halt\n");
			s.set(i+4, "S");

		}else{
			derivationPane.append("L>S :\t\t");
			derivationPane.append("begin ");
			derivationPane.append(arrayWord[i+1]+" ");
			derivationPane.append(arrayWord[i+2]+" ");
			derivationPane.append("S ");
			derivationPane.append(s.get(i+4)+ "\n");

			s.set(i+3, "S ");

			
	}

 }
 
 public void checkOb(int i) {
//	System.out.println(arrayLine.length);
	derivationPane.append("S --> obstacle T T\t");
	if(arrayLine.length == 3)
	{
		for(int j=0; j< s.size(); j++)
		{
			if(j==3)
			{
				derivationPane.append("obstacle ");
				derivationPane.append("T ");
				derivationPane.append("T ");
				derivationPane.append("halt\n");
				s.set(i+3, "");
				s.set(i+4, "");
			}
			derivationPane.append(s.get(j)+" ");
		}
	s.set(i+3, "obstacle");
	s.set(i+4, "T");
	derivationPane.append("T --> int\t\t");
	for(int j = 0; j<s.size();j++){
		derivationPane.append(s.get(j)+" ");
	}
	derivationPane.append(arrayWord[i+2]+" ");
	derivationPane.append("halt\n");
	s.add(i+5, arrayWord[i+2]);
	
	derivationPane.append("T --> int\t\t");
	for(int j = 0; j<s.size();j++){
		if(j==4){
			derivationPane.append(arrayWord[i+1]+" ");
			derivationPane.append(arrayWord[i+2]+" ");
			derivationPane.append("halt\n");
			s.set(i+4, "");
			s.set(i+5, "");
		}
		derivationPane.append(s.get(j)+" ");
	}
	s.set(i+4, arrayWord[i+1]);
	s.set(i+5, arrayWord[i+2]);
	s.add("halt");
	
	}else{
		
		for(int j=0; j< s.size(); j++)
		{
			if(j==3)
			{
				derivationPane.append("S ");
				derivationPane.append("obstacle ");
				derivationPane.append("T ");
				derivationPane.append("T ");
				derivationPane.append("halt\n");
				s.set(i+3, "");
				s.set(i+4, "");
			}
			derivationPane.append(s.get(j)+" ");
			System.out.println(s.get(j));
		}
	System.out.println(s.size());
	s.set(s.size()-2, "S");
	s.set(s.size()-1, "obstacle");
	s.add("T");
	derivationPane.append("T --> int\t\t");
	for(int j = 0; j<s.size();j++){
		derivationPane.append(s.get(j)+" ");
	}
	derivationPane.append(arrayWord[i+2]+" ");
	derivationPane.append("halt\n");
	s.add(i+5, arrayWord[i+2]);
	
	derivationPane.append("T --> int\t\t");
	for(int j = 0; j<s.size();j++){
		if(j==5){
			derivationPane.append(arrayWord[i+1]+" ");
			derivationPane.append(arrayWord[i+2]+" ");
			derivationPane.append("halt\n");
			s.set(i+4, "");
			s.set(i+5, "");
			s.set(i+6, "");
		}
		derivationPane.append(s.get(j)+" ");
//		System.out.println(s.get(j));
	}
	System.out.println(s.size());
	s.set(i+4, "obstacle");
	s.set(i+5, arrayWord[i+1]);
	s.add(arrayWord[i+2]);
	s.add("halt");
	}
}
 public void checkRobot(int i) {
	 derivationPane.append("S --> robot var T T\t");
	 if(arrayLine.length == 3)
	 {
		 for(int j=0; j< s.size(); j++)
			{
				if(j==3)
				{
					derivationPane.append("robot ");
					derivationPane.append(arrayWord[i+1]+" ");
					derivationPane.append("T ");
					derivationPane.append("T ");
					derivationPane.append("halt\n");
					s.set(i+3, "");
					s.set(i+4, "");
				}
				derivationPane.append(s.get(j)+" ");
			}
		 s.set(i+3, "robot");
		 s.set(i+4, arrayWord[i+1]);
		 
		 derivationPane.append("T --> int\t\t");
			for(int j = 0; j<s.size();j++){
				derivationPane.append(s.get(j)+" ");	
			}
			derivationPane.append("T"+" ");
			derivationPane.append(arrayWord[i+3]+" ");
			derivationPane.append("halt\n");
			s.add(i+5, arrayWord[i+2]);
			
			derivationPane.append("T --> int\t\t");
			for(int j = 0; j<s.size();j++){
				if(j==4){
					derivationPane.append(arrayWord[i+1]+" ");
					derivationPane.append(arrayWord[i+2]+" ");
					derivationPane.append(arrayWord[i+3]+" ");
					derivationPane.append("halt\n");
					s.set(i+4, "");
					s.set(i+5, "");
				}
				derivationPane.append(s.get(j)+" ");
			}
			s.set(i+4, arrayWord[i+1]);
			s.set(i+5, arrayWord[i+2]);
			s.add("halt");
	 }else{
		 s.remove("");
		 for(int j = 0; j<s.size();j++){
			 System.out.println(s.size());
			 if(j != s.size()-1){
				derivationPane.append(s.get(j)+" ");
			 }
			}
		 
		 for(int j=0; j< s.size(); j++)
			{
				if(j==3)
				{
					derivationPane.append("robot ");
					derivationPane.append(arrayWord[i+1]+" ");
					derivationPane.append("T ");
					derivationPane.append("T ");
					derivationPane.append("halt\n");

				}
			}
		 System.out.println(s.size());
		 s.set(s.size()-1, "robot");
		 s.add(arrayWord[i+1]);
		 
		 derivationPane.append("T --> int\t\t");
		 for(int j = 0; j<s.size();j++){
				derivationPane.append(s.get(j)+" ");	
			}
			derivationPane.append("T"+" ");
			derivationPane.append(arrayWord[i+3]+" ");
			derivationPane.append("halt\n");
			derivationPane.append("T --> int\t\t");
			
		for(int j = 0; j<s.size();j++){
					derivationPane.append(s.get(j)+" ");
					
				}
		derivationPane.append(arrayWord[i+2]+" ");
		derivationPane.append(arrayWord[i+3]+" ");
		derivationPane.append("halt\n");
		s.add(arrayWord[i+2]);
		s.add(arrayWord[i+3]);

	 }
	 
		
		}
		

 public void checkMove(int i) {
	 derivationPane.append("S --> move var D T\t");
	 if(arrayLine.length == 3)
	 {
		 for(int j=0; j< s.size(); j++)
			{
				if(j==3)
				{
					derivationPane.append("move ");
					derivationPane.append(arrayWord[i+1]+" ");
					derivationPane.append("D ");
					derivationPane.append("T ");
					derivationPane.append("halt\n");
					s.set(i+3, "");
					s.set(i+4, "");
				}
				derivationPane.append(s.get(j)+" ");
			}
		 s.set(i+3, "move");
		 s.set(i+4, arrayWord[i+1]);
		 
		 derivationPane.append("T --> int\t\t");
			for(int j = 0; j<s.size();j++){
				derivationPane.append(s.get(j)+" ");	
			}
			derivationPane.append("D"+" ");
			derivationPane.append(arrayWord[i+3]+" ");
			derivationPane.append("halt\n");
			s.add(i+5, arrayWord[i+2]);
			
			derivationPane.append("D -->"+ arrayWord[i+2] + "\t\t");
			for(int j = 0; j<s.size();j++){
				if(j==4){
					derivationPane.append(arrayWord[i+1]+" ");
					derivationPane.append(arrayWord[i+2]+" ");
					derivationPane.append(arrayWord[i+3]+" ");
					derivationPane.append("halt\n");
					s.set(i+4, "");
					s.set(i+5, "");
				}
				derivationPane.append(s.get(j)+" ");
			}
			s.set(i+4, arrayWord[i+1]);
			s.set(i+5, arrayWord[i+2]);
			s.add("halt");
	 }else{
		 s.remove(s.size()-8);
		 for(int j = 0; j<s.size();j++){
			 System.out.println(s.get(j));
			 if(j != s.size()){
				derivationPane.append(s.get(j)+" ");
			 }
			}
		 derivationPane.append("move ");
		 derivationPane.append(arrayWord[i+1]+" ");
		 derivationPane.append("D ");
		 derivationPane.append("T ");
		 derivationPane.append("halt\n");
		 s.add("move");
		 s.add(arrayWord[i+1]);
		 s.add("D");
		 s.add("T");
		 derivationPane.append("T --> int\t\t");
		 for(int j = 0; j<s.size();j++){
			 System.out.println(s.get(j));
			 if(j != s.size()-1){
				derivationPane.append(s.get(j)+" ");
			 }
			}
		 derivationPane.append(arrayWord[i+3]+" ");
		 derivationPane.append("halt\n");
		 derivationPane.append("D -->"+arrayWord[i+2]+"\t\t");
		 for(int j = 0; j<s.size();j++){
			 System.out.println(s.get(j));
			 if(j != s.size()-1 && j != s.size()-2){
				derivationPane.append(s.get(j)+" ");
			 }
			}
		 derivationPane.append(arrayWord[i+2]+" ");
		 derivationPane.append(arrayWord[i+3]+" ");
		 derivationPane.append("halt\n");
		 
	 }
	 
	
	 
}
 public void checkAdd(int i) {
	 derivationPane.append("S --> add T t v\t\t");
	 if(arrayLine.length == 3)
	 {
		 for(int j=0; j< s.size(); j++)
			{
				if(j==3)
				{
					derivationPane.append("add ");
					derivationPane.append("T ");
					derivationPane.append(arrayWord[i+2]+" " );
					derivationPane.append(arrayWord[i+3]+" " );
					derivationPane.append("halt\n");
					s.set(i+3, "");
					s.set(i+4, "");
				}
				derivationPane.append(s.get(j)+" ");
			}
		 s.set(i+3, "add");
		 s.set(i+4, arrayWord[i+1]);
			derivationPane.append("T -->int\t\t");
			for(int j = 0; j<s.size();j++){
				if(j==4){
					derivationPane.append(arrayWord[i+1]+" ");
					derivationPane.append(arrayWord[i+2]+" ");
					derivationPane.append(arrayWord[i+3]+" ");
					derivationPane.append("halt\n");
					s.set(i+4, "");
				
				}
				derivationPane.append(s.get(j)+" ");
			}
			s.set(i+4, arrayWord[i+1]);
			s.add("halt");
	 }
	
	 

	
}
// public boolean checkVariable(String s1){
//		if(s1.length() <= 8 && s1.length() > 1){
//			if (s1.matches("[a-zA-Z]+")){
//				return true;
//			}else return false;
//		}else if(s1.length() == 1){
//			if (s1.matches("[a-zA-Z]+")){
//				return true;
//			}else return false;
//		}else return false;
//	}
}