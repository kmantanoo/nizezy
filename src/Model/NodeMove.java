package Model;

import java.util.Stack;

public class NodeMove extends Node {
	private static final int valueCount = 3;
	public NodeMove(Stack<Token> syntaxValue) {
		super("move");
		loadValue(syntaxValue, valueCount);
	}
}
