package MayUse;
import java.util.ArrayList;

import Model.Derivation;
import Model.Error;
public class CompilerSyntax {
	private boolean checkError = true;
	private String[] arrLine , token;
	ArrayList<Error> error = new ArrayList<Error>();
	ArrayList<Derivation> derivationTable = new ArrayList<Derivation>();
	ArrayList<Rules> rulesTable = new ArrayList<Rules>();
	ArrayList<String> derivation = new ArrayList<String>();
	private String wordBegin,wordHalt;
	private String  L="L" ,S="S" , D="D";
	public CompilerSyntax() {
	}
	public void setLinesToken(String[] s){
		checkError = true;
		derivation.clear();
		derivationTable.clear();
		rulesTable.clear();
		error.clear();
		arrLine = s ;
		for(int i = 0 ; i<arrLine.length ; i ++){
			token = arrLine[i].trim().split("\\s+");
			if(i==0){
				wordBegin = token[0];
				System.out.println(wordBegin);
			}/*else if(i==1){
				wordBegin = token[0];
			}*/else if(i==arrLine.length -1){
				wordHalt = token[0];
				System.out.println(wordHalt);
			}
		}
	}
	public void compilerSyntax(){
		if(wordBegin.equals("begin")){
			token = arrLine[0].trim().split("\\s+");
			derivation.add("begin");
			derivation.add(token[1]);
			derivation.add(token[2]);
			//		derivation.add("begin");
			derivation.add(L);
			derivation.add("halt");
			derivationTable.add(new Derivation(derivation));
			rulesTable.add(new Rules("P>begin int int L halt"));
			if(wordHalt.equals("halt")){
				//check line
				if(arrLine.length == 3){
					for(int d=0 ; d<derivation.size() ; d++){
						if(derivation.get(d).equals("L")){
							derivation.set(d, S);
							derivation.add(d+1, ";");
						}
					}
					rulesTable.add(new Rules("L>S :\t"));
				}else{
					for(int d=0 ; d<derivation.size() ; d++){
						if(derivation.get(d).equals("L")){
							derivation.add(d+1, S);
							derivation.add(d+2, ";");
						}
					}
					rulesTable.add(new Rules("L>L S\t"));
				}
				derivationTable.add(new Derivation(derivation));
				for(int i=arrLine.length-2 ; i>0 ; i--){
					token = arrLine[i].trim().split("\\s+");
					//=======================================================================================
					if(token[0].equals("robot")){
						checkRobot();
					}else if(token[0].equals("obstacle")){
						checkObstacle();
					}else if(token[0].equals("move")){
						checkMove();
					}else if(token[0].equals("add")){
						checkAdd();
					}else if(checkVariable(token[0])){
						checkVar();
					}else if(token[0].equals("until")){
					//	checkDo();
					}
					//=============================================================================================
					if(i == 2){
						for(int d=0 ; d<derivation.size() ; d++){
							if(derivation.get(d).equals("L")){
								derivation.set(d, S);
								derivation.add(d+1, ";");
							}
						}
						rulesTable.add(new Rules("L>S ;\t"));
					}else{ for(int d=0 ; d<derivation.size() ; d++){
						if(derivation.get(d).equals("L")){
							derivation.add(d+1, S);
							derivation.add(d+2, ";");
						}
					}
					rulesTable.add(new Rules("L>L S ;\t"));
					}
					if(i != 1){
						derivationTable.add(new Derivation(derivation));
					}
				}
			}else {
				error.add(new Error("Syntax Error: size begin halt ","",arrLine.length));
				checkError = false;
			}
		}else {
			error.add(new Error("Syntax Error: size begin halt ","",1));
			checkError = false;
		}
	}
	public void checkRobot(){
		for(int d=0 ; d<derivation.size() ; d++){
			if(derivation.get(d).equals("S")){
				derivation.set(d, token[0]);
				derivation.add(d+1, token[1]);
				derivation.add(d+2, "T");
				derivation.add(d+3, "T");
				//	derivation.add(d+4, D);	
			}
		}
		derivationTable.add(new Derivation(derivation));
		rulesTable.add(new Rules("S>robot var T T \t"));
		// T 1
		for(int d=derivation.size()-1 ; d>0 ; d--){
			if(derivation.get(d).equals("T")){
				derivation.set(d, token[3]);
				break;
			}
		}
		derivationTable.add(new Derivation(derivation));
		rulesTable.add(new Rules("T>"+"int"+"\t"));
		// T 2 
		for(int d=0 ; d<derivation.size() ; d++){
			if(derivation.get(d).equals("T")){
				derivation.set(d, token[2]);
			}
		}
		derivationTable.add(new Derivation(derivation));
		rulesTable.add(new Rules("T>"+"int"+"\t"));
	}
	public void checkObstacle(){
		for(int d=0 ; d<derivation.size() ; d++){
			if(derivation.get(d).equals("S")){
				derivation.set(d, token[0]);
				derivation.add(d+1, "T");
				derivation.add(d+2, "T");	
			}
		}
		derivationTable.add(new Derivation(derivation));
		rulesTable.add(new Rules("S>obstacle T T \t"));
		// T 1
		for(int d=derivation.size()-1 ; d>0 ; d--){
			if(derivation.get(d).equals("T")){
				derivation.set(d, token[2]);
				break;
			}
		}
		derivationTable.add(new Derivation(derivation));
		rulesTable.add(new Rules("T>"+"int"+"\t"));
		// T 2 
		for(int d=0 ; d<derivation.size() ; d++){
			if(derivation.get(d).equals("T")){
				derivation.set(d, token[1]);
			}
		}
		derivationTable.add(new Derivation(derivation));
		rulesTable.add(new Rules("T>"+"int"+"\t"));
	}
	/*public void checkHole(){
		for(int d=0 ; d<derivation.size() ; d++){
			if(derivation.get(d).equals("S")){
				derivation.set(d, token[0]);
				derivation.add(d+1, token[1]);
				derivation.add(d+2, token[2]);	
			}
		}
		derivationTable.add(new DerivationTable(derivation));
		rulesTable.add(new RulesTable("S>hole int int\t"));
	}*/
	public void checkMove(){
		for(int d=0 ; d<derivation.size() ; d++){
			if(derivation.get(d).equals("S")){
				derivation.set(d, token[0]);
				derivation.add(d+1, token[1]);
				derivation.add(d+2, "D");
				derivation.add(d+3, "T");
			}
		}
		rulesTable.add(new Rules("S>move var D T\t"));
		derivationTable.add(new Derivation(derivation));
		// T
		for(int d=derivation.size()-1 ; d>0 ; d--){
			if(derivation.get(d).equals("T")){
				derivation.set(d, token[3]);
				break;
			}
		}
		derivationTable.add(new Derivation(derivation));
		rulesTable.add(new Rules("T>"+"int"+"\t"));
		// D
		for(int d=0 ; d<derivation.size() ; d++){
			if(derivation.get(d).equals("D")){
				derivation.set(d, token[2]);
			}
		}
		derivationTable.add(new Derivation(derivation));
		rulesTable.add(new Rules("D>"+token[2]+"\t"));
	}
	public void checkAdd(){
		for(int d=0 ; d<derivation.size() ; d++){
			if(derivation.get(d).equals("S")){
				derivation.set(d, token[0]);
				derivation.add(d+1, "T");
				derivation.add(d+2, "t");
				derivation.add(d+3, token[3]);
			}
		}
		rulesTable.add(new Rules("S>add T t v\t"));
		derivationTable.add(new Derivation(derivation));
		checkT(token[1]);
	}
	public void checkVar(){
		for(int d=0 ; d<derivation.size() ; d++){
			if(derivation.get(d).equals("S")){
				derivation.set(d, token[0]);
				derivation.add(d+1, "=");
				derivation.add(d+2, "T");
			}
		}
		rulesTable.add(new Rules("S>var = T\t"));
		derivationTable.add(new Derivation(derivation));
		checkT(token[2]);
	}
	/////
	public void checkT(String s){
		// T
		for(int d=derivation.size()-1 ; d>0 ; d--){
			if(derivation.get(d).equals("T")){
				derivation.set(d, s);
				break;
			}
		}
		derivationTable.add(new Derivation(derivation));
		if(checkInteger(s)){
			rulesTable.add(new Rules("T>"+"int"+"\t"));
		}else{
			rulesTable.add(new Rules("T>"+"var"+"\t"));
		}
	}
	/////
	//check Integer
	public boolean checkInteger(String s1) {
		if((s1.length() > 0 && s1.length() < 4) ){
			if (s1.matches("-?\\d+(\\.\\d+)?")  ){
				return true ;
			}else return false;
		}else return false;
	}
	//check variable
	public boolean checkVariable(String s1){
		if(s1.length() <= 8 && s1.length() > 1){
			if (s1.matches("[a-zA-Z]+")){
				return true;
			}else return false;
		}else if(s1.length() == 1){
			if (s1.matches("[a-zA-Z]+")){
				return true;
			}else return false;
		}else return false;
	}
	/////
	public boolean checkErrorSyntax(){
		return checkError;
	}
	public ArrayList<Error> getError(){
		return error;
	}
}